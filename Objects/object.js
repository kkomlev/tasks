function duplicateCount(text) {
    var obj = {};
    var count = 0;
    for (var i = 0; i < text.length; i++) {
        var t = text.charAt(i).toLowerCase();
        obj[t] = (t in obj === false) ?  1 : obj[t] + 1;
    }
    for (var key in obj) {
        if (obj[key] > 1) {
            count++;
        }
    } 
    return count;
};