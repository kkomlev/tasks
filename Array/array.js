function tribonacci(signature, n) {
    if (Array.isArray(signature) && Number.isInteger(n) && n > 0) {
        if (n < 3) {
            return signature;
        }
        for (var i=3; i < n; i++) {
            signature.push((signature[i - 1] + signature[i - 2] + signature[i - 3]));
        }
    } else {
        return [];
    };
    return signature;
};