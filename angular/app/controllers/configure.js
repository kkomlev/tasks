angular
    .module('MainModule', ['ngMaterial', 'ngRoute','ui.router'])
    .config(['$locationProvider','$stateProvider', function ($locationProvider,$stateProvider) {

    $stateProvider
        .state('login', {
            url: '/',
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl',
            controllerAs: 'loginClass'
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'templates/profile.html',
            controller: 'profileCtrl',
        })
    }]);