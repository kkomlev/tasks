angular
    .module('MainModule')

    .controller('profileCtrl', profileCtrl);

profileCtrl.$inject = ['$scope', '$location'];

function profileCtrl($scope, $location) {
    let user = JSON.parse(sessionStorage.getItem('user'));
    if (user !== null && user !== undefined) {
        $scope.firstName = user.fName;
        $scope.lastName = user.sName;
        $scope.login = user.log;
        $scope.email = user.adress;
        $scope.pass = user.password;
        $scope.repeatPass = user.password;
        sessionStorage.removeItem("user");
    }

    $scope.completeRegister = function () {
        if (user !== null && user !== undefined) {
            localStorage.removeItem(user.log);
        }
        localStorage.setItem($scope.login, JSON.stringify({
            fName: $scope.firstName,
            sName: $scope.lastName,
            log: $scope.login,
            adress: $scope.email,
            password: $scope.pass
        }));

        $location.path('/')
    };

    $scope.validLogin = function () {
        return $scope.myForm.login.$invalid = (localStorage.getItem($scope.login) !== null);
    };

    $scope.validEmail = function () {
        for (let key in localStorage) {
            if (localStorage.getItem(key) !== null && $scope.email === JSON.parse(localStorage.getItem(key)).adress) {
                return $scope.myForm.email.$invalid = true;
            }
        }
        return $scope.myForm.email.$invalid = false;
    };

}
