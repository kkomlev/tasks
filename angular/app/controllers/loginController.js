angular
    .module('MainModule')

    .controller('loginCtrl', loginCtrl);

loginCtrl.$inject = ['$location'];

function loginCtrl($location) {

    if (sessionStorage.getItem('check') !== null) {
        this.user = JSON.parse(sessionStorage.getItem('check'));
        this.remember = true;
        this.name = this.user.log;
        this.pass = this.user.password;
    }

    this.moveToProfile = function () {
        $location.path('/profile')
    };
    this.findLogin = function () {
        if (localStorage.getItem(this.name) === null) {
            return alert("Wrong login")
        }
        else {
            if (JSON.parse(localStorage.getItem(this.name)).password === this.pass) {
                if (this.remember === true) {
                    sessionStorage.clear();
                    sessionStorage.setItem('check', JSON.stringify({log: this.name, password: this.pass}));
                }
                else {
                    sessionStorage.clear();
                }
                sessionStorage.setItem('user', localStorage.getItem(this.name));
                this.moveToProfile();
            }
            else {
                return alert("Wrong pass")
            }
        }
    };
}