angular
    .module('MainModule', ['ngMaterial'])

    .controller('searchCtrl', function ($scope, $http, getImages,images) {

        $scope.startSearch = function () {
      getImages.searchImages($scope.searching);
            $scope.imageArray = images.array;
        };

    })


    .value('images',{})
    .service('getImages', function ($http, images) {
        this.searchImages = function (x) {
            return $http({
                method: 'GET',
                url: 'https://pixabay.com/api/',
                params: {
                    key: '9503560-9a80a756c7021cffdd3b521bb',
                    q: x
                }
            }).then(response => images.array = response.data.hits);

        };
    });