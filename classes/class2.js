"use strict;"

function romanToArabic(num) {
    const map = new Map([['I', 1], ['V', 5], ['X', 10], ['L', 50], ['C', 100], ['D', 500], ['M', 1000]]);
    num = num.split("").reduce(function (result, current, i, num) {
        current = (map.get(num[i - 1]) < map.get(current)) ? map.get(current) - map.get(num[i - 1]) * 2 : map.get(current);
        return result + current;
    }, 0);

    return num;
}

const numberProxy = new Proxy(Number.prototype, {
    get(target, property) {

        if (!(property in target)) {
            const arr = [];
            for (let i = 0; i < romanToArabic(property); i++) {
                arr.push(i);
            }
            return target[property] = arr;
        }
        return false;
    }
});

//console.log(numberProxy.XVIV);
//console.log(Object.getOwnPropertyNames(Number.prototype));
