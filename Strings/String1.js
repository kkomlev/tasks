//Return the number (count) of vowels in the given string.


function getCount(str) {
  var vowelsCount = 0;
  var vowelsMasive =["e"," u "," i "," o "," a "," E "," U "," I "," O "," A "];
 	for ( i = 0; i < str.length; i++ ){
 		for ( j = 0; j < vowelsMasive.length; j++ ) {
  			if (str.charAt( i ) === vowelsMasive[ j ] ) {
  				vowelsCount++;
 			}
 		}  
  	}
  return vowelsCount;
};
