function longest(s1, s2) {
    var newStr = s1.concat(s2);
    var arrayChar = check(newStr).split("").sort();

    function check(str) {
        var newChars = "";
        for (var i = 0; i < str.length; i++) {
            if ( newChars.indexOf(str[i]) === -1 ) {
                newChars += str[i];
            }
        }
        return newChars;
    }
    return arrayChar.join("");
};



