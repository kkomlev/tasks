function sum(x) {

    if (x === undefined) {
        x = 0;
    }

    nextsum.valueOf = function() {
        return x;
    };

    function nextsum(y) {
        if (Number.isInteger(y)) {
            x += y;
        }
        return nextsum;
    };
    return nextsum;
};
//Realize functions sum with next signatures
//7     console.log( +sum()(1)(2)(3)()()(2) );
//8    console.log( +sum()(1)(2)(3)()()(1) );
