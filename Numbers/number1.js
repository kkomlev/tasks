var isSquare = function(num){
    var sqrtedNum = (Math.sqrt(num));

    return sqrtedNum === 0 || num % sqrtedNum === 0;
};
